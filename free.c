#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <mach/mach.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <math.h>

char *VERSION = "0.1";

void usage(){
    fprintf(stderr,"Usage:\n");
    fprintf(stderr," free [option]\n\n");
    fprintf(stderr,"Options:\n");
    fprintf(stderr," -b, --bytes\t show output in bytes\n");
    fprintf(stderr," -k, --kilo\t show output in kilobytes\n");
    fprintf(stderr," -m, --mega\t show output in megabytes\n");        
    fprintf(stderr," -g, --giga\t show output in gigabytes\n");        
    fprintf(stderr," -t, --tera\t show output in terabytes\n");        
    fprintf(stderr," -p, --peta\t show output in petabytes\n");
    fprintf(stderr," -h, --help\t display help and exit\n");            
}

void version(){
    fprintf(stderr, "free for macOS %s\n",VERSION);
}

void get_stats(vm_statistics64_t stat)
{
    unsigned int count = HOST_VM_INFO64_COUNT;
    kern_return_t ret;
    if ((ret = host_statistics64(mach_host_self(), HOST_VM_INFO64, (host_info64_t)stat, &count) != KERN_SUCCESS)) {
        fprintf(stderr, "failed to get statistics. error %d\n", ret);
        exit(EXIT_FAILURE);
    }
}

int64_t get_physical_memory(){
    int mib[2] = {CTL_HW, HW_MEMSIZE};
    size_t length = sizeof(int64_t);
    int64_t physical_memory;
    sysctl(mib, 2, &physical_memory, &length, NULL, 0);
    return physical_memory;
}

void get_xsw(struct xsw_usage *xsw){
    int mib[2] = {CTL_VM, VM_SWAPUSAGE};
    size_t xswlen = sizeof(*xsw);
    sysctl(mib, 2, xsw, &xswlen, NULL, 0);
}

int get_magic_number(char unit){
    int64_t magic_number = 1;
    switch(unit){
    case 'b':
        return magic_number;
    case 'k':
        return magic_number * 1024;
    case 'm':
        return magic_number * 1024 * 1024;
    case 'g':
        return magic_number * 1024 * 1024 * 1024;
    case 't':
        return magic_number * 1024 * 1024 * 1024 * 1024;
    case 'p':
        return magic_number * 1024 * 1024 * 1024 * 1024 * 1024;
    }
    return magic_number;
}

void print_free(char unit){
    int64_t magic_number = get_magic_number(unit);
    vm_statistics64_data_t vm_stat; get_stats(&vm_stat);
    struct xsw_usage xsw;           get_xsw(&xsw); // swap struct
    typedef union mem_stats_u {
        struct mem_stats_s {
            uint64_t total,      used,      free,
                     wire,       in_active, spec,
                     comp,   purge,
	             swap_total, swap_used, swap_free;
        } mem_stats_s;
        uint64_t mem_stats_a[10];
    } mem_stats;
    mem_stats stats = {
        get_physical_memory() / magic_number,
        (vm_stat.wire_count + vm_stat.throttled_count + vm_stat.active_count) * vm_kernel_page_size / magic_number,
        (vm_stat.free_count - vm_stat.speculative_count) * vm_kernel_page_size / magic_number,
        (vm_stat.wire_count + vm_stat.throttled_count) * vm_kernel_page_size / magic_number,
        (vm_stat.active_count + vm_stat.inactive_count) * vm_kernel_page_size / magic_number,
        vm_stat.speculative_count * vm_kernel_page_size / magic_number,
        vm_stat.compressor_page_count * vm_kernel_page_size / magic_number,
        vm_stat.purgeable_count * vm_kernel_page_size / magic_number,
        xsw.xsu_total / magic_number,
        xsw.xsu_used / magic_number,
        (xsw.xsu_total - xsw.xsu_used) / magic_number
    };

    int col_width = 8; // longest title + 1 (inac/ac)
    for(int i = 0; i < sizeof(union mem_stats_u)/sizeof(uint64_t); i++){
      int num_digits = log10(stats.mem_stats_a[i]) + 1;
      if ( col_width <= num_digits )
          col_width = num_digits + 1;
    };

    printf("      %-*s%-*s%-*s%-*s%-*s%-*s%-*s%-*s\n",
	   col_width, "total",
	   col_width, "used",
	   col_width, "free",
	   col_width, "wire",
	   col_width, "inac/ac",
	   col_width, "spec",
	   col_width, "comp",
	   col_width, "purge"
    );
    printf("Mem:  %-*llu%-*llu%-*llu%-*llu%-*llu%-*llu%-*llu%-*llu\n",
	   col_width, stats.mem_stats_s.total,
	   col_width, stats.mem_stats_s.used,
	   col_width, stats.mem_stats_s.free,
	   col_width, stats.mem_stats_s.wire,
	   col_width, stats.mem_stats_s.in_active,
	   col_width, stats.mem_stats_s.spec,
	   col_width, stats.mem_stats_s.comp,
	   col_width, stats.mem_stats_s.purge
    );
    printf("Swap: %-*llu%-*llu%-*llu\n",
	   col_width, stats.mem_stats_s.swap_total,
	   col_width, stats.mem_stats_s.swap_used,
	   col_width, stats.mem_stats_s.swap_free
    );
}
  
int main(int argc, char * const *argv) {
    static struct option longopts[] = {
        { "bytes",   no_argument, NULL, 'b' },
        { "kilo",    no_argument, NULL, 'k' },
        { "mega",    no_argument, NULL, 'm' },
        { "giga",    no_argument, NULL, 'g' },
        { "tera",    no_argument, NULL, 't' },
        { "peta",    no_argument, NULL, 'p' },
        { "help",    no_argument, NULL, 'h' },
	{ "version", no_argument, NULL, 'V' }
    };
    
    int c;
    int bflag = 0;
    int kflag = 0;
    int mflag = 0;
    int gflag = 0;
    int tflag = 0;
    int pflag = 0;
    int enabled_flags = 0;
    while ((c = getopt_long(argc, argv, "bkmgthpV :", longopts, NULL)) != -1) {
        switch(c){
	case 'b':
	    bflag = 1;
	    enabled_flags += 1;
            break;
	case 'k':
	    kflag = 1;
	    enabled_flags += 1;	    
            break;
	case 'm':
	    mflag = 1;
	    enabled_flags += 1;	    
            break;
	case 'g':
	    gflag = 1;
	    enabled_flags += 1;	    
            break;
	case 't':
	    tflag = 1;
	    enabled_flags += 1;	    
            break;
	case 'p':
	    pflag = 1;
	    enabled_flags += 1;	    
            break;
	case 'h':
	    usage();
            exit(0);
	case 'V':
	    version();
            exit(0);	    
	default:
	    bflag = 1;
	    enabled_flags += 1;	    
            break;
        }
    }
    
    if (enabled_flags > 1){
        fprintf(stderr,"error: Too many options\n");
        usage();
        exit(1);
    }
    if (bflag)
      print_free('b');
    else if (kflag)
      print_free('k');
    else if (mflag)
      print_free('m');
    else if (gflag)
      print_free('g');
    else if (tflag)
      print_free('t');
    else if (pflag)
      print_free('p');
    else
      print_free('b');
}
