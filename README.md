# macOS free
A free-like command for macOS. Inspired by the `free` command distributed with GNU/Linux.

## Installation
Installs to /usr/local/bin by default:
```
make && sudo make install
```

Can be installed to an alternate location (ex. ~/.local/bin) by specifying a PREFIX:
```
make && make install PREFIX=~/.local
```

## Examples
### Print memory usage in megabytes
```
$ free -m
      total   used    free    wire    inac/ac spec    comp    purge
Mem:  16384   7028    647     1643    10642   155     2641    440
Swap: 1024    28      996
```
### Getting help
```
$ free -h
Usage:
 free [option]

Options:
 -b, --bytes	 show output in bytes
 -k, --kilo	 show output in kilobytes
 -m, --mega	 show output in megabytes
 -g, --giga	 show output in gigabytes
 -t, --tera	 show output in terabytes
 -p, --peta	 show output in petabytes
 -h, --help	 display help and exit
 ```
